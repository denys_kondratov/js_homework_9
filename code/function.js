let counter = 0, minutes = 0, hours = 0, intervalHandler, flag = false;

const get = id => document.getElementById(id);

const count = () => {
   if(counter<10){
      get("outputSeconds").innerHTML = `0${counter}`;
   }else{
      get("outputSeconds").innerHTML = counter;
   }
   counter++;
   if(counter==60){
      minutes++;
      if(minutes<10){
         get("outputMinutes").innerHTML = `0${minutes}`;
      }else{
         get("outputMinutes").innerHTML = minutes;
      }
      counter = 0;
      if(minutes==60){
         hours++;
         if(hours<10){
            get("outputHours").innerHTML = `0${hours}`;
         }else{
            get("outputHours").innerHTML = hours;
         }
         minutes = 0;
      }
   }
}

get("startButton").onclick = () => {
   get("stopwatchDisplay").classList.remove("red");
   if(!flag){
      intervalHandler = setInterval(count, 1000);
      flag = true;
   }else{
      console.error("таймер вже запущений!");
   }
}

get("stopButton").onclick = () => {
   clearInterval(intervalHandler);
   flag = false;
   get("stopwatchDisplay").classList.add("red");
}

get("resetButton").onclick = () => {
   hours = 0;
   minutes = 0;
   counter = 0;
   get("outputHours").innerHTML = `0${hours}`;
   get("outputMinutes").innerHTML = `0${minutes}`;
   get("outputSeconds").innerHTML = `0${counter}`;
}

let pattern = /\d{3}-\d{3}-\d{2}-\d{2}/,
flagPhone = false;

get("buttonCheck").onclick = () => {
   let checked_number = get("phoneCheck").value;
   if(pattern.test(checked_number)){
      const div = document.querySelector("#phone div");
      div.remove();
      get("phoneCheck").classList.add("green");
      setTimeout(redir, 500);
   }else{
      if(flagPhone == false){
         const div = document.createElement("div");      
         div.textContent = "Формат номеру не вірний!";
         div.classList.add("red");
         get("phone").prepend(div);
         flagPhone = true;
      }
   }
}

const redir = () => {
   document.location.href = "https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg";
}